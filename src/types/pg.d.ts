export interface PgKBDataItemType {
  id: string;
  q: string;
  a: string;
  user_id: string;
  kb_id: string;
}
